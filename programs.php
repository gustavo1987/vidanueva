<?php /* Template Name: Programs */ ?>
<?php get_header() ?>
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><a href="">Inicio</a></li>
                    <li>Programas</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="radio-programs section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Nuestros programas de radio</h2>
                    <p>Contamos con diversos programas que despiertan con buen humor a los oyentes y los mantienen bien informados. </p> 
                </div>
            </div>
        </div>





        <div class="row justify-content-center">

            <?php $programas=get_field("programas");?>
            <?php foreach ($programas as $value): ?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-radio-program">
                    <img src="<?php echo $value["imagen"]["url"]; ?>" alt="">
                    <h3><a href=""><?php echo $value["nombre"]; ?></a></h3>
                    <div class="program-meta">
                        <span><?php echo $value["horario"]; ?></span>
                        <span><?php echo $value["dias_programa"]; ?></span>
                        <span><a href="rj-profile.html"><?php echo $value["categoria"]; ?></a></span>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>



        <!--div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-1.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-2.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-3.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-4.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-5.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-6.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.7s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-7.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.7s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-8.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-9.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                <div class="single-radio-program">
                    <img src="assets/images/single-radio-program-10.jpg" alt="">
                    <h3><a href="">Sunday Night Special</a></h3>
                    <div class="program-meta">
                        <span>10.00 PM</span>
                        <span>Sunday to Friday</span>
                        <span><a href="rj-profile.html">RJ Nixon</a></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.4s">
                <a href="" class="bttn-mid btn-fill">Load More</a>
            </div>
        </div-->
    </div>
</section>
<?php get_footer()?>