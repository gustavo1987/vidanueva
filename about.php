<?php /* Template Name: About */ ?>
<?php get_header() ?>
        <section class="custom-banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="custom-banner-content">
                            <h2>Acerca de Vida Nueva</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--section class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>">Inicio</a></li>
                            <li>Acerca de Vida Nueva</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section-->
        <section class="about-area section-padding-2">
            <div class="container">
                <div class="row">

                    <?php $programas=get_field("section_about");?>
                    <?php foreach ($programas as $value): ?>
                        <?php if($value["direction"]=="right"):?>

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
                                <div class="about-content-img">
                                    <img src="<?= $value["image"]["url"];?>" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
                                <div class="about-content">
                                    <div class="section-title-left cl-black">
                                        <h2><?= $value["title"];?></h2>
                                        <?= $value["text"];?>
                                        
                                    </div>
                                </div>
                            </div>
                        <?php elseif($value["direction"]=="left"):?>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
                                <div class="about-content">
                                    <div class="section-title-left cl-black">
                                        <h2><?= $value["title"];?></h2>
                                        <p class="text-justify"><?= $value["text"];?></p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
                                <div class="about-content-img">
                                    <img src="<?= $value["image"]["url"];?>" alt="">
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>



                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="about-content history">
                            <div class="section-title-left cl-black">
                                <h2>Nuestra Historia</h2>
                                <?php echo get_field("history");?>
                                <!--p class="text-justify">La visión de Dios A.C., conocida comercialmente como Vida nueva la radio de la familia, es una asociación civil formada por la familia Pérez Chan, en la ciudad de Mérida, Yucatán.</p>
                                <p class="text-justify">Fue en el año 2014, que Dios puso en nuestros corazones la idea de solicitar esta estación de radio, con la intención de orientar a cada familia, debido a las diferentes necesidades presentadas en cada una de ellas. Algunas de estas son:</p>
                                <ul class="ul-history">
                                    <li class="list-history">Familias desintegradas por diversas causas.</li>
                                    <li class="list-history">Padres de familia y jóvenes en adicciones; (drogas, alcoholismo, pornografía, entre otras.)</li>
                                    <li class="list-history">Jóvenes en total rebeldía y que faltan el respeto a sus padres.</li>
                                    <li class="list-history">Abuso sexual a menores de edad.</li>
                                </ul>
                                <p class="text-justify">Presenciamos con frecuencia el dolor en familias por suicidio y pérdida de un ser querido.</p>
                                <p class="text-justify">Según estadísticas del INEGI, Yucatán ocupa el primer lugar en suicidios en el País, principalmente en jóvenes con un rango de entre 15 a 29 años de edad. El secretario de Salud Estatal, consideró que el problema no solo le atañe al sector Salud, pues influyen varios factores, como, la desintegración del núcleo familiar, el desempleo, adicciones, depresión, y situaciones de extrema pobreza.</p>
                                <p class="text-justify"> Cabe señalar que lograr esta estación de radio no fue fácil, al tener nuestro primer acercamiento con el IFT. Instituto federal de telecomunicaciones, nos informo que nuestro municipio no contaba con capacidad espectral, esto, nos llevó a solicitar durante dos años 2015 Y 2016 que Mérida, Yucatán fuera incluido en el programa anual de uso y aprovechamiento de bandas de frecuencias.</p>
                                <p class="text-justify">Fue así, que en el año 2017 nuestro municipio ya contaba con capacidad espectral, procedimos de inmediato a presentar nuestra solicitud formal la cual nos llevó otros dos años de espera en los cuales  nuestra solicitud y finalmente el 22 de mayo del presente año nuestro sueño es una realidad.</p>
                                <p class="text-justify"> Sabiendo que la familia es la base de la sociedad y el pertenecer a una es de suma importancia en el desarrollo psicológico y social del individuo, nuestro propósito es que esta estación de radio, a través de una propuesta de contenidos educativos y con el apoyo de un grupo de especialistas como, consejeros familiares, psicólogos, maestros en educación, abogados, doctores, entre otros, se haga presente como una nueva opción, llevando a cada hogar una barra programática, con segmentos variados, que promuevan la unidad familiar y rescaten valores morales que ayuden a la formación del carácter en niños y jóvenes, para lograr así un mejor desempeño en las diferentes esferas de la sociedad.</p-->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="radio-jockey-area section-padding-2 gray-bg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                        <div class="section-title cl-black">
                            <h2>Nuestros Conductores</h2>
                            <p>Estos son nuestros conductores que amenan nuestros programas con su alegria y sus conocimiento a transmitir.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php $conductores=get_field("conductores");?>
                    <?php foreach ($conductores as $value): ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="single-radio-jockey">
                            <img src="<?php echo $value["imagen"]["url"];?>" alt="<?php echo $value["nombre"]; ?>">
                            <div class="radio-jockey-des">
                                <h4><?php echo $value["nombre"];?></h4>
                                <p><?php echo $value["programa"];?></p>
                                <div class="rj-social">
                                    <?php if($value["facebook"]): ?>
                                        <a href="<?php echo $value["facebook"];?>"><i class="fa fa-facebook"></i></a>
                                    <?php endif;?>
                                    <?php if($value["twitter"]):?>
                                        <a href="<?php echo $value["twitter"];?>"><i class="fa fa-twitter"></i></a>
                                    <?php endif;?>
                                    <?php if($value["instagram"]):?>
                                        <a href="<?php echo $value["instagram"];?>"><i class="fa fa-instagram"></i></a>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
        </section>
<?php get_footer()?>