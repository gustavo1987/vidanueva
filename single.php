<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage VidaNueva
 * @since Vida Nueva 1.0
 */

get_header(); ?>
        <!--section class="custom-banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="custom-banner-content">
                            <h2>Blog de Vida Nueva</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section-->
        <section class="blog-details-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-7 wow fadeInLeft" data-wow-delay="0.3s">
                        <div class="blog-details-content">
                            <img src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="" />
                            <div class="blog-meta">
                                <p><a href=""><i class="fa fa-user"><?php echo get_the_author_meta("description");?></i></a><a href=""><i class="fa fa-heart"></i>215</a><a href=""><i class="fa fa-comment"></i>17</a><a href=""><i class="fa fa-calendar"></i></a></p>
                            </div>
                            <h2 class="cl-black"><?php echo the_title();?></h2>
                            <?php print_r(get_post()->post_content); ?>
                            <!--div class="blog-tag-share mt-50">
                                <div class="blog-tag">
                                    <p><i class="fa fa-tags"></i><a href="">Radio</a>, <a href="">Studio</a>, <a href="">Live show</a> , <a href="">Radio News</a>, <a href="">Live Studio</a></p>
                                </div>
                                <div class="blog-share">
                                    <i class="fa fa-share"></i><a href=""><i class="fa fa-facebook-f"></i></a><a href=""><i class="fa fa-twitter"></i></a><a href=""><i class="fa fa-linkedin"></i></a><a href=""><i class="fa fa-instagram"></i></a>
                                </div>
                            </div-->
                            <div class="fb-comments" data-href="http://vidanueva.c-developers.com/" data-width="100%" data-numposts="5"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 wow fadeInRight" data-wow-delay="0.3s">
                        <div class="page-sidebar">
                            <div class="single-sidebar-block most-viewed-post">
                                <h3>Más populares</h3>
                                <div class="viewed-post">
                                    <div class="row">
                                        <?php $postvida=$post_list = get_posts( array(
                                                'numberposts'   => 4, 
                                                'post_status'   => 'publish',
                                                'orderby'       =>  'date',
                                                'order'         =>  'DESC'
                                            ));
                                        ?>

                                        <?php foreach ($postvida as $key => $value):?>
                                        <div class="col-6">
                                            <a href="<?php echo get_permalink($value->ID); ?>" class="single-most-viewed-post mb-30">
                                                <img src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="">
                                                <h4><?php echo $value->post_title;?></h4>
                                            </a>
                                        </div>
                                        <?php endforeach;?>
                                        <!--div class="col-6">
                                            <a href="" class="single-most-viewed-post mb-30">
                                                <img src="assets/images/most-viewed-post2.jpg" alt="">
                                                <h4>Another as studied it to evident</h4>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="" class="single-most-viewed-post">
                                                <img src="assets/images/most-viewed-post3.jpg" alt="">
                                                <h4>The Dangerous Truth About Clinton</h4>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="" class="single-most-viewed-post">
                                                <img src="assets/images/most-viewed-post4.jpg" alt="">
                                                <h4>Mr my ready guest ye after short at</h4>
                                            </a>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php get_footer(); ?>