<?php /* Template Name: Programs */ ?>
<?php get_header() ?>

<section class="radio-programs section-padding">
    <div class="container">
		<div class="d-flex justify-content-center align-items-center" id="main">
		    <h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">404</h1>
		    <div class="inline-block align-middle">
		    	<h2 class="font-weight-normal lead" id="desc">No se encontró la página solicitada.</h2>
		    </div>
		</div>
	</div>
</section>
<?php get_footer()?>