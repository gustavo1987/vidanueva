<?php /* Template Name: Home */ ?>
<?php get_header();
        echo do_shortcode('[smartslider3 slider=2]');
?>
<section class="hero-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <audio id="urario-radio" controls="" autoplay="autoplay"><source src="http://149.56.195.94:8545/;stream.mp3" type="audio/mp3">Su navegador no soporta el elemento de audio.</audio>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="hero-content">
                    <h2><?php echo get_field("titulo_inicio");?></h2>
                    <p><?php echo get_field("texto_inicio");?></p>
                    <a href="radio-programs.html" class="bttn-mid btn-fill">Ver Programas</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="radio-programs section-padding-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Nuestros programas de radio</h2>
                    <p>Contamos con diversos programas que despiertan con buen humor a los oyentes y los mantienen bien informados. </p> 
                </div>
            </div>
        </div>
        <div class="row justify-content-center">

            <?php $programas=get_field("programas");?>
            <?php foreach ($programas as $value): ?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-radio-program">
                    <img src="<?php echo $value["imagen"]["url"]; ?>" alt="">
                    <h3><a href=""><?php echo $value["nombre"]; ?></a></h3>
                    <div class="program-meta">
                        <span><?php echo $value["horario"]; ?></span>
                        <span><?php echo $value["dias_programa"]; ?></span>
                        <span><a href="rj-profile.html"><?php echo $value["categoria"]; ?></a></span>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<section class="radio-jockey-area section-padding-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Conductores</h2>
                    <p>Estos son algunos de nuestros conductores, conocelos.</p>
                </div>
            </div>
        </div>
        <div class="row">

            <?php $conductores=get_field("conductores");?>
            <?php foreach ($conductores as $value): ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                <div class="single-radio-jockey">
                    <img src="<?php echo $value["imagen"]["url"];?>" alt="<?php echo $value["nombre"]; ?>">
                    <div class="radio-jockey-des">
                        <h4><?php echo $value["nombre"];?></h4>
                        <p><?php echo $value["programa"];?></p>
                        <div class="rj-social">
                        	<?php if($value["facebook"]): ?>
                            	<a href="<?php echo $value["facebook"];?>"><i class="fa fa-facebook"></i></a>
                        	<?php endif;?>
                        	<?php if($value["twitter"]):?>
                            	<a href="<?php echo $value["twitter"];?>"><i class="fa fa-twitter"></i></a>
                        	<?php endif;?>
                        	<?php if($value["instagram"]):?>
                            	<a href="<?php echo $value["instagram"];?>"><i class="fa fa-instagram"></i></a>
                        	<?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
        </div>
    </div>
</section>
<section class="program-schedule section-padding dark-overlay" style="background: url('assets/images/program-bg.jpg') fixed;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-white">
                    <h2>Horario diario del programa</h2>
                    <p>Estos son nuestros programas por horarios y por días.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-30 wow fadeInLeft" data-wow-delay="0.4s">
                <div class="nav flex-column nav-pills program-schedule-navs" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <?php $dias=get_field("dias");?>
                    <?php foreach ($dias as $key => $value):?>
                    <a class="nav-link <?php echo $key==0?'active':'';?>" id="<?php echo $value["nombre_dia"];?>-tab" data-toggle="pill" href="#<?php echo $value["nombre_dia"];?>" role="tab" aria-controls="<?php echo $value["nombre_dia"];?>" aria-selected="true"><?php echo $value["nombre_dia"];?></a>
                	<?php endforeach;?>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
                <div class="tab-content program-schedule-content" id="v-pills-tabContent">
                    <?php $dias=get_field("dias");?>
                    <?php foreach ($dias as $key => $value):?>
                    <div class="tab-pane fade show <?php echo $key==0?'active':'';?> table-responsive table-responsive-sm table-responsive-xs" id="<?php echo $value["nombre_dia"];?>" role="tabpanel" aria-labelledby="<?php echo $value["nombre_dia"];?>-tab">
                        <table class="table table-borderless">
                          <thead>
                            <tr>
                              <th scope="col">Hora</th>
                              <th scope="col">Programa</th>
                              <th scope="col">Tipo</th>
                              <th scope="col">Conductor</th>
                            </tr>
                          </thead>
                          <tbody>
		                    <?php foreach ($value["horarios_programa"] as $key => $value2):?>
                                <tr>
                                  <td style="padding: .15rem .55rem;"><?php echo $value2["hora"];?></td>
                                  <td style="padding: .15rem .55rem;"><?php echo $value2["programa"];?></td>
                                  <td style="padding: .15rem .55rem;"><?php echo $value2["categoria"];?></td>
                                  <td style="padding: .15rem .55rem;"><?php echo $value2["conductor"];?></td>
                                </tr>
                			<?php endforeach;?>
                          </tbody>
                        </table>
                      </div>
                  <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="portfolio-area section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Galería de foto</h2>
                    <p>Estas son algunas de nuestras fotos de todo nuestros programas.</p>
                </div>
            </div>
        </div>
        <div class="text-center mb-40 wow fadeInUp" data-wow-delay="0.4s">
            <ul class="portfolio-filter">
                <li class="active"><a href="#" data-filter="*"> Todos</a></li>
                <?php $dias=get_field("categoria_galeria");?>
                <?php foreach ($dias as $key => $value):?>
                  <li><a href="#" data-filter=".<?php echo $key;?>"><?php echo $value["nombre_categoria_galeria"];?></a></li>
                <?php endforeach;?>
                <!--li><a href="#" data-filter=".cat2">De mujer a mujer <span>03</span></a></li>
                <li><a href="#" data-filter=".cat3">Vida de casados  <span>04</span></a></li>
                <li><a href="#" data-filter=".cat4">Restauremos familias <span>03</span></a></li-->
            </ul>
        </div>
        <div class="row portfolio portfolio-gallery column-3 gutter wow fadeInUp" data-wow-delay="0.5s">

            <?php $dias=get_field("categoria_galeria");?>
            <?php foreach ($dias as $key => $value):?>
              <?php foreach ($value["imagenes_galeria"] as $key2 => $value2):?>
                <?php if($value2["type"]=="IMAGEN"):?>

                  <?php if($value2["type_imagen"]=="HORIZONTAL"):?>
                    <div class="portfolio-item <?php echo $key;?> ">
                        <a href="<?php echo $value2["imagen"]["url"];?>" class="thumb popup-gallery" title="">
                            <img src="<?php echo $value2["imagen"]["url"];?>" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-description">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>

                  <?php elseif($value2["type_imagen"]=="VERTICAL"):?>

                    <div class="portfolio-item <?php echo $key;?> ">
                        <a href="<?php echo $value2["imagen"]["url"];?>" class="thumb popup-gallery" title="">
                            <img src="<?php echo $value2["imagen"]["url"];?>" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-description">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                  <?php endif;?>
                <?php elseif($value2["type"]=="VIDEO"):?>
                  <?php if($value2["type_imagen"]=="HORIZONTAL"):?>
                    <div class="portfolio-item <?php echo $key;?> ">
                      <a href="<?php echo $value2["video"];?>" class="thumb video-popup" title="">
                        <img src="<?php echo $value2["imagen"]["url"];?>" alt="">
                        <div class="portfolio-hover">
                            <div class="portfolio-description">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.png" alt="">
                            </div>
                        </div>
                      </a>
                    </div>
                    <?php elseif($value2["type_imagen"]=="VERTICAL"):?>
                      <div class="portfolio-item <?php echo $key;?> ">
                        <a href="<?php echo $value2["video"];?>" class="thumb video-popup" title="">
                          <img src="<?php echo $value2["imagen"]["url"];?>" alt="">
                          <div class="portfolio-hover">
                              <div class="portfolio-description">
                                  <img src="<?php echo $value2["imagen"]["url"];?>" alt="">
                              </div>
                          </div>
                        </a>
                      </div>
                    <?php endif;?>
                <?php endif;?>
              <?php endforeach;?> 
            <?php endforeach;?>
        </div>
    </div>
</section>
<section class="latest-news section-padding-2 gray-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Últimas noticias</h2>
                    <p>Nuestras últimas noticias para toda tu familia</p>
                </div>
            </div>
        </div>
        <div class="row">

            <?php   $postvida=$post_list = get_posts( array(
                        'numberposts'   => 2, 
                        'post_status'   => 'publish',
                        'orderby'       =>  'date',
                        'order'         =>  'DESC'
                    ));
            ?>
            <?php foreach ($postvida as $key => $value):?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-blog">
                    <img src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="">
                    <div class="blog-meta-content">
                        <div class="blog-meta">
                            <a href=""><i class="fa fa-user"></i><?php echo $value->post_author;?></a>
                            <a href=""><i class="fa fa-heart"></i>214</a>
                            <a href=""><i class="fa fa-comment"></i><?php echo $value->comment_count;?></a>
                        </div>
                        <h2><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title;?></a></h2>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <!--div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
                <div class="single-blog">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/blog-2.jpg" alt="">
                    <div class="blog-meta-content">
                        <div class="blog-meta">
                            <a href=""><i class="fa fa-user"></i>Luisa</a>
                            <a href=""><i class="fa fa-heart"></i>214</a>
                            <a href=""><i class="fa fa-comment"></i>15</a>
                        </div>
                        <h2><a href="">Las mejores playlist de música</a></h2>
                    </div>
                </div>
            </div-->
        </div>
    </div>
</section>
<section class="partner-logo-area section-padding-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Nuestros patrocinadores</h2>
                    <p>Estos son nuestros patrocinadores y su página para visitarlos.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                <div class="single-partners owl-carousel">


                <?php $patrocinador=get_field("patrocinador");?>
                <?php foreach ($patrocinador as $key => $value):?>
                    <a href="<?php echo $value['url_patrocinador'];?>"><img src="<?php echo $value['imagen_patrocinador']["url"];?>" alt=""></a>
                <?php endforeach;?>

                     <!--a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/partner-2.jpg" alt=""></a>
                     <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/partner-3.jpg" alt=""></a>
                     <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/partner-1.jpg" alt=""></a>
                     <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/partner-2.jpg" alt=""></a-->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="partner-logo-area section-padding-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Redes Sociales</h2>
                    <p>Sigue nuestras redes sociales.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <h4 class="text-center">Facebook</h4>
                <div class="fb-page" data-href="https://www.facebook.com/vidanuevalaradiodelafamilia" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/vidanuevalaradiodelafamilia" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/vidanuevalaradiodelafamilia">Vida Nueva la radio de la familia</a></blockquote></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                <h4 class="text-center">Twitter</h4>
                <a class="twitter-timeline" href="https://twitter.com/MexicoNueva?ref_src=twsrc%5Etfw">Tweets by MexicoNueva</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
    </div>
</section>
<?php get_footer()?>