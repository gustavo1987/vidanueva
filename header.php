<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="revisit-after" content="7 days">
        <meta name="googlebot"      content="follow, index, all">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="vida nueva, radio de la familia, noticias familia, tips familia, radio familia, mexico" />
        <meta name="author" content="www.c-developers.com" />
        <meta name="copyright" content="2019 Vida Nueva Todos los derechos reservados." />
        <meta name="application-name" content="Vida Nueva" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" type="image/x-icon">
        <title>Vida Nueva - La Radio de la Familia</title>
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/magnific-popup.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php wp_head();?>
    </head>
    <body>
        <!--API de facebook-->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=186998448375859&autoLogAppEvents=1"></script>
        <!--Fin API de Facebook -->

		<div class="preloader">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/preloader.png" alt="">
            <h4>Vida Nueva</h4>
		</div>
        <header class="header-area">
            <nav class="navbar sticky-top navbar-expand-lg main-menu">
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-completo.png" class="d-inline-block align-top" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu-toggle"></span>
                    </button>
                    <?php
                        wp_nav_menu( array( 
                            'theme_location' => 'my-custom-menu', 
                            'container_class' => 'collapse navbar-collapse',
                            'container_id' => 'navbarSupportedContent',
                            /*'container' => 'ul',*/
                            'menu_class'=> 'navbar-nav m-auto') ); 
                        ?>
                    <!--div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item"><a class="nav-link" href="<?php echo get_template_directory_uri(); ?>">Inicio</a></li>
                            <li class="nav-item"><a class="nav-link" href="about.html">Acerca de</a></li>
                            <li class="nav-item"><a class="nav-link" href="radio-programs.html">Programas</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="contact.html">Contactanos</a></li>
                        </ul>
                        <div class="menu-btn justify-content-end"><a href="#sponsor" class="bttn-small btn-emt">Suscribete</a></div>
                    </div-->
                </div>
            </nav>
        </header>