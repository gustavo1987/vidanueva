<?php /* Template Name: Blog */ ?>
<?php get_header();?>
<!--section class="custom-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="custom-banner-content">
                    <h2>Nuestras noticias</h2>
                </div>
            </div>
        </div>
    </div>
</section-->
<section class="latest-news section-padding-2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 centered wow fadeInUp" data-wow-delay="0.3s">
                <div class="section-title cl-black">
                    <h2>Nuestras noticias</h2>
                    <p>Estos son nuestros noticias</p>
                </div>
            </div>
        </div>
        <div class="row">
            <?php   $postvida=$post_list = get_posts( array( 
                        'post_status'   => 'publish',
                        'orderby'       =>  'date',
                        'order'         =>  'DESC'
                    ));
            ?>
            <?php foreach ($postvida as $key => $value):?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-blog">
                    <img src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="<?php echo $value->post_title;?>">
                    <div class="blog-meta-content">
                        <div class="blog-meta">
                            <a href="<?php echo get_permalink($value->ID); ?>"><i class="fa fa-user"></i><?php echo get_the_author($value->post_author);?></a>
                            <a href="<?php echo get_permalink($value->ID); ?>"><i class="fa fa-heart"></i>214</a>
                            <a href="<?php echo get_permalink($value->ID); ?>"><i class="fa fa-comment"></i><?php echo $value->comment_count;?></a>
                        </div>
                        <h2><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title;?></a></h2>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php get_footer()?>