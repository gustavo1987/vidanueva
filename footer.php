<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <!--div class="footer-menu">
                    <ul>
                        <li><a href="">Inicio</a></li>
                        <li><a href="">Acerca de</a></li>
                        <li><a href="">Programas</a></li>
                        <li><a href="">Blog</a></li>
                        <li><a href="">Contactanos</a></li>
                    </ul>
                </div-->

                <?php
                        wp_nav_menu( array( 
                            'theme_location' => 'my-custom-menu', 
                            'container_class' => 'footer-menu') ); 
                        ?>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="footer-social">
                    <a href="https://www.facebook.com/vidanuevalaradiodelafamilia" target="_black"><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/scrollUp.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/magnific-popup.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
    <?php wp_footer(); ?>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5d28cca2bfcb827ab0cb86c3/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>