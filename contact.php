<?php /* Template Name: Contact */ ?>
<?php get_header();?>
<!--section class="custom-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="custom-banner-content">
                    <h2>Contactanos</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><a href="">Inicio</a></li>
                    <li>Contactanos</li>
                </ul>
            </div>
        </div>
    </div>
</section-->
<section class="contact-form section-padding">
    <form action="#" id="contact_form_submit">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 centered wow fadeInUp" data-wow-delay="0.3s">
                    <div class="section-title cl-black">
                        <h2>Contactanos</h2>
                        <p>Dejanos tu comentario, enviandonos un mensaje en el formulario. </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 centered">
                    <?php echo do_shortcode('[contact-form-7 id="200" title="Formulario de contacto 1" html_class="row"]'); ?>
                    <!--div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <input type="text" placeholder="Nombre*">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <input type="text" placeholder="Apellidos*">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <input type="email" placeholder="Email*">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <input type="text" placeholder="Teléfono">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <textarea name="rj-msg" rows="4" placeholder="Tu mensaje"></textarea>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="bttn-mid btn-fill">Envíar mensaje</button>
                        </div>
                    </div-->
                </div>
            </div>

            
        </div>
    </form>
</section>
<div id="map" class="dark-overlay"></div>
<?php get_footer()?>